#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""Program description
@author: Andrei Matveyeu
@organization: ideabulbs.com
@license: GNU GPL
@contact: andrei@ideabulbs.com

memonerd is a Linux text-mode tool for flashcard learning using the spaced repetition method.

"""
import datetime
import sqlite3
import random

class Flashcard(object):
	"""Objective: a class representing a flashcard
	@ivar _id: instance id
	@type _id: C{int}
	@ivar _question: flashcard question
	@type _question: C{str}
	@ivar _answer: flashcard answer
	@type _answer: C{str}
	@ivar _last_repetition: the L{Repetition} with which the Flashcard was scheduled/repeated last time
	@type _last_repetition: L{Repetition}
	@ivar _next_repetition: the outstanding L{Repetition} for the Flashcard
	@type _next_repetition: L{Repetition}

	@todo: flashcard priority, repetition cost algorithm (based on repetition length)
	"""
	_id = 0
	_question = ""
	_answer = ""
	_last_repetition = None
	_next_repetition = None
	
	def __init__(self):
		pass
	
	# classmethod 
	def from_sqlite_dict(self, **sqlite_dict):
		self.__dict__.update(sqlite_dict)
	
	def get_id(self):
		"""Get flashcard id
		@return: flashcard id
		@rtype: C{int}
		"""
		return self._id

	def set_id(self, i):
		"""Set flashcard id
		@param i: new id
		@type i: C{str}
		"""
		self._id = i

	def get_last_repetition(self):
		"""Get the id of the repetition with which the flashcard was last repeated
		@return: repetition
		@rtype: L{Repetition}
		"""
		return self._last_repetition

	def set_last_repetition(self, r):
		"""Set the repetition with which the flashcard was last repeated
		@param r: repetition id
		@type r: L{Repetition}
		"""
		self._last_repetition = r

	def get_next_repetition(self):
		"""Get the next repetition for this flashcard
		@return: repetition
		@rtype: L{Repetition}
		"""
		return self._next_repetition

	def set_next_repetition(self, r):
		"""Set the next repetition for this flashcard
		@param r: repetition
		@type r: L{Repetition}
		"""
		self._next_repetition = r

	def get_question(self):
		"""Get the question
		@return: question
		@rtype: C{str}
		"""
		return self._question

	def set_question(self, q):
		"""Set the question
		@param q: new question
		@type q: C{str}
		"""
		self._question = q

	def get_answer(self):
		"""Get the answer
		@return: answer
		@rtype: C{str}
		"""
		return self._answer

	def set_answer(self,a):
		"""Set the answer
		@param a: new answer
		@type a: C{str}
		"""
		self._answer = a

	def __str__(self):
		"""String representation of a Flashcard instance
		"""
		return "Flashcard(q='" + self._question + "'; a='" + self._answer + "'; id=" + str(self._id) + ")"

class Repetition(object):
	"""Objective: a class representing a repetition
	@ivar _scheduled_datetime: datetime when the repetition was scheduled for
	@type _scheduled_datetime: C{datetime}
	@ivar _done_datetime: datetime when the repetition was done
	@type _done_datetime: C{datetime}
	@ivar _duration: repetition duration in seconds
	@type _duration: C{float}
	@ivar _grade: repetition grade
	@type _grade: C{float}
	@ivar _answer: answer given during the repetition
	@type _answer: C{str}
	@ivar _id: the id of the repetition. C{0} if the instance has not been serialized to the database yet.
	@type _id: C{int}
	@ivar _priority: repetition priority. A flashcard may have varying priorities over time.
	@type _priority: C{float}
	@ivar _flashcard: the L{Flashcard} instance which the repetition is bound to
	@type _flashcard: L{Flashcard}
	"""
	# classmethod 
	def from_sqlite_dict(self, **sqlite_dict):
		self.__dict__.update(sqlite_dict)
		if self._scheduled_datetime:
			self._scheduled_datetime = datetime.datetime.strptime(self._scheduled_datetime, "%Y-%m-%d %H:%M")
		if self._done_datetime:
			self._done_datetime = datetime.datetime.strptime(self._done_datetime, "%Y-%m-%d %H:%M")
	
	def __init__(self):
		"""Default constructor"""
		self._scheduled_datetime = None
		self._done_datetime = None
		self._duration = None
		self._answer = ""
		self._grade = None
		self._id = 0
		self._priority = 0
		
	def get_id(self):
		"""Get repetition id.

		{0} if the instance has not been serialized to the database
		yet. The id is the primary key assigned by the database.
		@return: repetition id
		@rtype: C{int}
		"""
		return self._id

	def set_id(self, i):
		"""Set new repetition id
		@param i: new id
		@type i: C{int}
		"""
		self._id = i

	def get_flashcard(self):
		"""Get flashcard
		
		@return: flashcard
		@rtype: L{Flashcard}
		"""
		return self._flashcard

	def set_flashcard(self, f):
		"""Set flashcard
		@param f: new flashcard
		@type f: L{Flashcard}
		"""
		self._flashcard = f

	def get_priority(self):
		"""Get the priority
		@return: priority
		@rtype: C{float}
		"""
		return self._priority

	def set_priority(self, p):
		"""Set the priority
		@param p: priority
		@type p: C{float}
		"""
		self._priority = p


	def get_scheduled_datetime(self):
		"""Get scheduled datetime

		For an upcoming repetition this is the datetime when the
		repetition I{becomes due}.

		For a past repetition this is the datetime when it I{was due}.
		"""
		return self._scheduled_datetime

	def set_scheduled_datetime(self, new_datetime):
		"""Set scheduled datetime
		@param new_datetime: datetime when the repetition becomes due
		@type new_datetime: C{datetime}
		"""
		self._scheduled_datetime = new_datetime


	def get_done_datetime(self):
		"""Get datetime when the repetition was done
		
		Notes:
			- C{None} is returned if the instance is an upcoming repetition
		"""
		return self._done_datetime

	def set_done_datetime(self, new_datetime):
		"""Set datetime when the repetition was done
		@param new_datetime: datetime 
		@type new_datetime: C{datetime}
		"""
		self._done_datetime = new_datetime


	def get_duration(self):
		"""Get the duration of the repetition
		
		Notes:
			- C{None} is returned if this is an upcoming repetition 

		@return: duration
		@rtype: C{float}
		"""
		return self._duration

	def set_duration(self, d):
		"""Set duration of the current repetition
		@param d: new duration
		@type d: C{float}
		"""
		self._duration = d


	def get_answer(self):
		"""Get answer given during the repetition
		@return: answer
		@rtype: C{str}
		"""
		return self._answer

	def set_answer(self, a):
		"""Set answer given during the repetition
		@param a: new answer
		@type a: C{str}
		"""
		self._answer = a

	def get_grade(self):
		"""Get grade given as the result of the repetition
		@return: grade
		@rtype: C{float}
		"""
		return self._grade

	def set_grade(self, g):
		"""Set grade given as the result of the repetition
		@param g: new grade
		@type g: C{float}
		"""
		self._grade = g

	def is_done(self):
		"""Indicate whether the repetition has been done or is an outstanding one.

		@return: C{True} if repetition was done. C{False} if repetition was not done (is an outstanding one).
		@rtype: C{bool}
		"""
		if self._grade != None:
			return True
		else:
			return False

	def __str__(self):
		"""String representation of a Repetition instance
		"""
		f = "None"
		if self._flashcard:
			f = self._flashcard.get_id()
		if self.is_done():
			return "Repetition(flashcard=" + str(f) + "; answer='" + self._answer + "'; done=" + str(self._done_datetime) +"; grade=" + str(self._grade)  + ")"
		else:
			return "Repetition(flashcard=" + str(f) + "'; scheduled=" + str(self._scheduled_datetime) + ")"

class ApplicationLogic(object):
	"""Application logic handler: interaction between UserInterface, DatabaseHandler and RepetitionHandler
	"""
	def __init__(self):
		pass

class RepetitionHandler(object):
	"""Objective: a class managing repetitions:
		- grade repetitions
		- schedule new repetitions
	"""
	def __init__(self):
		pass
	
	def get_next_repetition(self, repetitions):
		"""Objective: calculate next scheduled interval based on the provided repetitions list
		@param repetitions: list of L{Repetition} instances
		@type repetitions: C{list}
		@return: next scheduled repetition
		@rtype: L{Repetition}
		"""
		repetition_count = len(repetitions)
		
		if repetition_count < 1:
			return None
		
		flashcard = repetitions[-1].get_flashcard()
		last_priority = repetitions[-1].get_priority()
		last_grade = repetitions[-1].get_grade()

		if last_grade > 2.0:
			last_interval = self.get_last_done_interval(repetitions)
			if last_interval == 0:
				last_interval = 10
			new_interval = last_interval * 1.3
			delta = datetime.timedelta(seconds=new_interval * 3600)
		else:
			delta = datetime.timedelta(seconds=86400)
		
		new_datetime = datetime.datetime.now() + delta
			
		repetition = Repetition()
		repetition.set_flashcard(flashcard)
		repetition.set_scheduled_datetime(new_datetime)
		repetition.set_priority(last_priority)
		
		return repetition
		
	def get_last_done_interval(self, repetitions):
		"""Objective: get the interval in days between the last two done repetitions in a row
		"""
		if len(repetitions) < 2:
			return 0
		date2 = repetitions[-1].get_done_datetime()
		date1 = repetitions[-2].get_done_datetime()
		result = date2 - date1
		result = result.total_seconds() / 3600 
		return result

	def get_last_scheduled_interval(self, repetitions):
		"""Objective: get the interval in days between the last two done repetitions in a row
		"""
		if len(repetitions) < 2:
			return 0
		date2 = repetitions[-1].get_scheduled_datetime()
		date1 = repetitions[-2].get_scheduled_datetime()
		result = date2 - date1
		result = result.total_seconds() / 3600 
		return result


	def get_average_interval(self, repetitions):
		"""Objective: calculate the average interval for a sequence of repetitions
		@param repetitions: list of L{Repetition} instances
		@type repetitions: C{list}
		"""

class DatabaseHandler(object):
	"""Objective: a class managing database IO
	"""
	def __init__(self):
		pass

	def _dict_factory(self, cursor, row):
		"""Objective: a factory used to return database poll results as a dictionary
		"""
		d = {}
		for idx, col in enumerate(cursor.description):
			d[col[0]] = row[idx]
		return d

	def connect(self):
		"""Objective: connect to the database: initialize connection reference and cursor
		"""
		self._conn = sqlite3.connect('exampledb.sqlite3')
		self._conn.row_factory = self._dict_factory
		self._cursor = self._conn.cursor()
		self._cursor.execute("PRAGMA foreign_keys = ON")

	def get_flashcard_by_id(self, flashcard_id, get_repetitions=True):
		"""Objective: get a flashcard from the database by its id
		
		Notes:
			- When this method is called by any other method except L{get_repetition_by_id}, the Flashcard instance will always contain at least one L{Repetition} object
			- When this method is called by L{get_repetition_by_id}, the C{last_repetition} and C{next_repetition} fields will contain C{None} to avoid recursion

		@return: C{Flashcard} with the given id or C{None}
		@rtype: L{Flashcard}
		"""
		self._cursor.execute('SELECT * FROM Flashcard WHERE _id=%d' % (flashcard_id))
		result = self._cursor.fetchone()
		if result:
			card = Flashcard()
			card.from_sqlite_dict(**result)
			if get_repetitions:
				last_repetition = self.get_repetition_by_id(result['_last_repetition_id'], get_flashcard=False)
				next_repetition = self.get_repetition_by_id(result['_next_repetition_id'], get_flashcard=False)
				card.set_last_repetition(last_repetition)
				card.set_next_repetition(next_repetition)
			return card
		else:
			return None

	def get_repetition_by_id(self, repetition_id, get_flashcard=True):
		"""Objective: get a repetition from the database by its id
		
		Notes:
			- When this method is called by any other method except L{get_flashcard_by_id}, the Repetition instance will always contain a L{Flashcard} object
			- When this method is called by L{get_flashcard_by_id}, the C{_flashcard} field will contain C{None} to avoid recursion
				
		@return: C{Repetition} with the given id or C{None}
		@rtype: L{Repetition}
		"""
		self._cursor.execute('SELECT * FROM Repetition WHERE _id=%d' % (repetition_id))
		result = self._cursor.fetchone()
		if result:
			repetition = Repetition()
			repetition.from_sqlite_dict(**result)
			if get_flashcard:
				flashcard = self.get_flashcard_by_id(result['_flashcard_id'], get_repetitions=False)
				repetition.set_flashcard(flashcard)
			return repetition
		else:
			return None

	def update_flashcard(self, flashcard):
		self._cursor.execute('UPDATE Flashcard SET _answer="%s", _question="%s", _next_repetition_id=%d, _last_repetition_id=%d WHERE _id=%d' %(flashcard.get_answer(), flashcard.get_question(), flashcard.get_next_repetition().get_id(), flashcard.get_last_repetition().get_id(), flashcard.get_id()))

	def delete_flashcard(self, flashcard):
		self._cursor.execute('DELETE FROM Flashcard  WHERE _id=%d' % (flashcard.get_id()))
		
	def update_repetition(self, repetition):
		self._cursor.execute('UPDATE Repetition SET _answer="%s", _duration=%f, _priority=%f, _grade=%f, _done_datetime="%s" WHERE _id=%d' % (repetition.get_answer(), repetition.get_duration(), repetition.get_priority(), repetition.get_grade(), datetime.datetime.now(), repetition.get_id()))

	def get_next_outstanding_flashcard(self):
		self._cursor.execute('SELECT id, _flashcard_id FROM Repetition WHERE _grade IS NULL AND  _scheduled_datetime BETWEEN "2012-10-04 00:00" AND "2012-10-04 23:59"')
		result = self._cursor.fetchone()
		if not result:
			return None
		card_id = result['flashcard_id']
		card = self.get_flashcard_by_id(card_id)
		card.set_last_repetition_id(result['id'])
		return card

	def get_done_repetitions_for_flashcard(self, flashcard):
		"""Objective: get done repetitions for the given L{Flashcard} instance
		"""
		self._cursor.execute('SELECT * FROM Repetition WHERE _flashcard_id=%d AND _grade IS NOT NULL' %(flashcard.get_id()))
		query_result = self._cursor.fetchall()
		print(len(query_result))
		result = []
		for r in query_result:
			repetition = Repetition()
			repetition.from_sqlite_dict(**r)
			repetition.set_flashcard(flashcard)
			result.append(repetition)
		return result

	def get_scheduled_repetitions_for_flashcard(self, flashcard):
		"""Objective: get scheduled repetitions for the given L{Flashcard} instance
		"""
		self._cursor.execute('SELECT * FROM Repetition WHERE _flashcard_id=%d AND _done_datetime IS NULL' %(flashcard.get_id()))
		query_result = self._cursor.fetchall()
		print(len(query_result))
		result = []
		for r in query_result:
			repetition = Repetition()
			repetition.from_sqlite_dict(**r)
			repetition.set_flashcard(flashcard)
			result.append(repetition)
		return result
	
	def store_flashcard(self, flashcard):
		pass
	
	def store_repetition(self, repetition):
		self._cursor.execute("""INSERT INTO Repetition(_answer, _grade, _duration, _priority, _done_datetime)
	                            VALUES ("%s", %f, %f, %f, "%s")""" % (repetition.get_answer(), repetition.get_grade(), repetition.get_duration(), repetition.get_priority(), repetition.get_datetime()))


	def get_scheduled_repetition_count_for_date(self, date):
		"""Objective: return the number of repetitions scheduled for the given date
		"""
		self._cursor.execute('SELECT count(_id) FROM Repetition WHERE _grade IS NULL AND  _scheduled_datetime BETWEEN "' + date.__str__() + ' 00:00" AND "' + date.__str__() +' 23:59"')
		result = self._cursor.fetchone()
		if not result:
			return None
		return result['count(_id)']

	def get_repetition_count(self):
		self._cursor.execute('SELECT count(_id) FROM Repetition')
		result = self._cursor.fetchone()
		return result['count(_id)']

class UI(object):
	def __init__(self):
		pass

	def show_question(self, question):
		print("Q: " + question)

	def get_answer(self):
		print("A: ")
		answer = input()
		return answer

	def run(self):
		dbh = DatabaseHandler()
		dbh.connect()
		card = dbh.get_next_outstanding_flashcard()
		self.show_question(card.get_question())
		answer = self.get_answer()

		grade = 0.0
		if answer == card.get_answer():
			grade = 1.0
		else:
			print("WRONG!")

		repetition = Repetition()
		repetition.set_id(card.get_last_repetition_id())
		repetition.set_answer(answer)
		repetition.set_grade(grade)
		dbh.update_repetition(repetition)
		dbh.schedule_new_repetition(card.get_id()) #
		dbh.get_repetition_count()

def main():
	"""Main method"""
	ui = UI()
	ui.run()
	pass

if __name__ == "__main__":
	main()

