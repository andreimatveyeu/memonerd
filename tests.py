#!/usr/bin/python3
import unittest
import random
import subprocess
import datetime
import sqlite3
from memonerd import Flashcard, Repetition, DatabaseHandler, RepetitionHandler

class TestRepetitionHandler(unittest.TestCase):
	# TODO: repetition priorities and costs
	def setUp(self):
		self._dbh = DatabaseHandler()
		tdbh = TestDatabaseHandler()
		tdbh._populate_table()
		self._dbh.connect()
		
	def tearDown(self):
		subprocess.call(['rm', '-f', 'exampledb.sqlite3'])
		pass
	
	def test_get_last_done_interval(self):
		"""Objective: functional test to check that proper last done interval is returned
		
		Approval criteria:
			- a correct last done interval is returned for a list of two repetitions
		"""
		rh = RepetitionHandler()
		repetition1 = Repetition()
		repetition1.set_done_datetime(datetime.datetime(2011, 1, 1, 18, 0))
		repetition1.set_grade(1.0)
		
		repetition2 = Repetition()
		repetition2.set_done_datetime(datetime.datetime(2011, 1, 3, 18, 0))
		repetition1.set_grade(2.0)

		repetitions = []
		repetitions.append(repetition1)
		repetitions.append(repetition2)

		self.assertEqual(rh.get_last_done_interval(repetitions), 48, "Verifying the done interval")

	def test_get_last_scheduled_interval(self):
		"""Objective: functional test to check that proper last scheduled interval is returned
		
		Approval criteria:
			- a correct last scheduled interval is returned for a list of two repetitions
		"""
		rh = RepetitionHandler()
		repetition1 = Repetition()
		repetition1.set_scheduled_datetime(datetime.datetime(2011, 1, 1, 18, 0))
		repetition1.set_grade(1.0)
		
		repetition2 = Repetition()
		repetition2.set_scheduled_datetime(datetime.datetime(2011, 1, 5, 18, 0))
		repetition1.set_grade(2.0)

		repetitions = []
		repetitions.append(repetition1)
		repetitions.append(repetition2)

		self.assertEqual(rh.get_last_scheduled_interval(repetitions), 96, "Verifying the done interval")

	
	def test_get_next_repetition(self):
		"""Objective: functional test to check that repetition scheduling works correctly
		
		Approval criteria:
			- next repetition is correctly scheduled and returned
		"""
	
		flashcard = self._dbh.get_flashcard_by_id(2, get_repetitions=False)
		repetitions = self._dbh.get_done_repetitions_for_flashcard(flashcard)
		
		rh = RepetitionHandler()
	
		next_repetition = rh.get_next_repetition(repetitions)
		
		print(next_repetition)

		self.assertEqual(next_repetition.get_scheduled_datetime() > datetime.datetime.now(), True, "Verifying scheduled repetition datetime")
		self.assertEqual(next_repetition.get_flashcard().get_id(), flashcard.get_id(), "Checking flashcard id")

class TestDatabaseHandler(unittest.TestCase):
	def setUp(self):
		subprocess.call(['rm', '-f', 'exampledb.sqlite3'])
		self._populate_table()
		self._dbh = DatabaseHandler()
		self._dbh.connect()

	def tearDown(self):
		subprocess.call(['rm', '-f', 'exampledb.sqlite3'])
		pass

	def _dict_factory(self, cursor, row):
		"""Objective: a factory used to return database poll results as a dictionary
		"""
		d = {}
		for idx, col in enumerate(cursor.description):
			d[col[0]] = row[idx]
		return d

	def _populate_table(self):
		"""Objective: fill the database with sample data necessary during the development
		"""
		conn = sqlite3.connect('exampledb.sqlite3')
		conn.row_factory = self._dict_factory
		cursor = conn.cursor()
		
		cursor.execute("PRAGMA foreign_keys = ON")
		cursor.execute("""CREATE TABLE Flashcard (
		_id	 INTEGER PRIMARY KEY NOT NULL,
		_last_repetition_id int,
		_next_repetition_id int,
		_question	text,
		_answer	  text,
		FOREIGN KEY(_last_repetition_id) REFERENCES Repetition(_id) ON DELETE CASCADE,
		FOREIGN KEY(_next_repetition_id) REFERENCES Repetition(_id) ON DELETE CASCADE
		)""")

		cursor.execute("""CREATE TABLE Repetition (
    	_id     INTEGER PRIMARY KEY NOT NULL,
		_flashcard_id  int,
		_scheduled_datetime DATETIME,
		_done_datetime DATETIME,
		_duration float,
		_answer text,
		_grade   float,
		_priority   float,
		FOREIGN KEY(_flashcard_id) REFERENCES Flashcard(_id) ON DELETE CASCADE
		)""")

		cursor.execute("""INSERT INTO Flashcard(_question, _answer) VALUES ("question1", "answer1")	""")
		cursor.execute("""INSERT INTO Flashcard(_question, _answer) VALUES ("question2", "answer2")	""")

		cursor.execute("""INSERT INTO Repetition(_flashcard_id, _scheduled_datetime, _grade, _priority) VALUES (1, "2012-10-01 12:00", NULL, 2.5)""")
		cursor.execute("""INSERT INTO Repetition(_flashcard_id, _answer, _done_datetime, _grade, _duration, _priority) VALUES (1, "wrong-answer1", "2012-10-03 12:00", 1.0, 5.0, 3.5)""")
		cursor.execute("""INSERT INTO Repetition(_flashcard_id, _answer, _done_datetime, _grade, _duration, _priority) VALUES (2, "answer2",  "2012-10-07 12:00", 1.0, 5.0, 4.5)""")
		cursor.execute("""INSERT INTO Repetition(_flashcard_id, _answer, _done_datetime, _grade, _duration, _priority) VALUES (2, "answer2", "2012-10-12 12:00", 1.0, 5.0, 5.5)""")

		cursor.execute("UPDATE Flashcard SET _last_repetition_id=2 WHERE _id=1")
		cursor.execute("UPDATE Flashcard SET _next_repetition_id=1 WHERE _id=1")

		conn.commit()
		conn.close()
		
	
	def test_get_flashcard_by_id(self):
		"""Objective: functional test to check that a Flashcard can be read from the database
		
		Approval criteria:
			- an object of type L{Flashcard} is returned by L{DatabaseHandler.get_flashcard_by_id}
		"""
		f = self._dbh.get_flashcard_by_id(1)
		self.assertEqual(f.get_question(), "question1", "Check that the question is 'question1'")
		self.assertEqual(f.get_answer(), "answer1", "Check that the answer is 'answer1'")
		self.assertEqual(type(f), type(Flashcard()), "Verify the type of the received object")
		self.assertEqual(type(f.get_last_repetition()), type(Repetition()), "Check that the Flashcard has a last repetition instance")
		self.assertEqual(type(f.get_next_repetition()), type(Repetition()), "Check that the Flashcard has a next repetition instance")


	def test_get_repetition_by_id(self):
		"""Objective: functional test to check that a Flashcard can be read from the database
		
		Approval criteria:
			- an object of type L{Flashcard} is returned by L{DatabaseHandler.get_flashcard_by_id}
		"""
		r = self._dbh.get_repetition_by_id(1)
		self.assertEqual(r.get_id(), 1, "Check that the id is 1")
		self.assertEqual(type(r), type(Repetition()), "Verify the type of the received object")
		self.assertEqual(type(r.get_flashcard()), type(Flashcard()), "Check that the repetition has a Flashcard instance")
		
	def test_update_flashcard(self):
		"""Objective: functional test to check that a Flashcard can be updated
		
		Approval criteria:
			- an L{Flashcard} instance is successfully updated by L{DatabaseHandler.update_flashcard}
		"""
		new_answer = "updated answer"
		new_question = "updated question"
		f = self._dbh.get_flashcard_by_id(1)
		f.set_answer(new_answer)
		f.set_question(new_question)
		
		self._dbh.update_flashcard(f)
		f2 = self._dbh.get_flashcard_by_id(1)
		self.assertEqual(f2.get_answer(), new_answer, "Verify that the flashcard answer has been updated")
		self.assertEqual(f2.get_question(), new_question, "Verify that the flashcard answer has been updated")

	def test_delete_flashcard(self):
		"""Objective: functional test to check that a Flashcard instance can be deleted from database
		
		Approval criteria:
			- an L{Flashcard} instance is deleted on cascade with its associated repetitions
		"""
		repetition_count = self._dbh.get_repetition_count()
		f = self._dbh.get_flashcard_by_id(1)
		self._dbh.delete_flashcard(f)
		new_repetition_count = self._dbh.get_repetition_count()
		self.assertEqual(repetition_count, new_repetition_count + 2, "Checking that associated repetitions have been removed on cascade")

	def test_get_done_repetitions_for_flashcard(self):
		"""Objective: functional test to check that done repetitions can be retrieved for a Flashcard
		
		Approval criteria:
			- a list of L{Repetition} instances (_done_datetime != NULL) is returned
		"""
		f = self._dbh.get_flashcard_by_id(1, get_repetitions=True)
		repetitions = self._dbh.get_done_repetitions_for_flashcard(f)
		self.assertEqual(len(repetitions), 1, "Verify the number of repetitions")
		self.assertEqual(repetitions[0].get_id(), 2, "Check repetition id")
		self.assertEqual(repetitions[0].get_answer(), "wrong-answer1", "Check repetition answer")
		self.assertEqual(repetitions[0].get_duration(), 5.0, "Check duration")
		self.assertEqual(repetitions[0].get_priority(), 3.5, "Check priority")
		self.assertEqual(type(repetitions[0].get_done_datetime()), datetime.datetime, "Check datetime type")

	def test_get_scheduled_repetitions_for_flashcard(self):
		"""Objective: functional test to check that done repetitions can be retrieved for a Flashcard
		
		Approval criteria:
			- a list of L{Repetition} instances (_done_datetime == NULL) is returned
		"""
		f = self._dbh.get_flashcard_by_id(1, get_repetitions=True)
		repetitions = self._dbh.get_scheduled_repetitions_for_flashcard(f)
		self.assertEqual(len(repetitions), 1, "Verify the number")
		self.assertEqual(repetitions[0].get_id(), 1, "Check repetition id")
		self.assertEqual(repetitions[0].get_answer(), None, "Check repetition answer")
		self.assertEqual(type(repetitions[0].get_scheduled_datetime()), datetime.datetime, "Check datetime type")

	def test_get_scheduled_repetition_count_for_date(self):
		"""Objective: functional test to check that scheduled repetition count is returned correctly
		
		Approval criteria:
			- the L{DatabaseHandler} returns the correct number of repetitions scheduled for the given date
		"""
		date = datetime.date(2012, 10, 1)
		repetition_count = self._dbh.get_scheduled_repetition_count_for_date(date)
		self.assertEqual(repetition_count, 1, "Verifying repetition count")
	

class TestRepetition(unittest.TestCase):
	def test_repetition_set_get_id(self):
		"""Objective: functional test to check that id set/get operations are consistent"""
		repetition = Repetition()
		new_id = random.randint(0, 100000)
		repetition.set_id(new_id)
		assert repetition.get_id() == new_id

	def test_repetition_set_get_grade(self):
		"""Objective: functional test to check that grade set/get operations are consistent"""
		repetition = Repetition()
		new_grade = random.random()
		repetition.set_grade(new_grade)
		assert repetition.get_grade() == new_grade

	def test_repetition_set_get_duration(self):
		"""Objective: functional test to check that duration set/get operations are consistent"""
		repetition = Repetition()
		new_duration = random.random()
		repetition.set_duration(new_duration)
		assert repetition.get_duration() == new_duration

	def test_repetition_set_get_answer(self):
		"""Objective: functional test to check that answer set/get operations are consistent"""
		repetition = Repetition()
		new_answer = random.choice(["answer1", "answer2", "answer3"])
		repetition.set_answer(new_answer)
		assert repetition.get_answer() == new_answer

	def test_repetition_set_get_scheduled_datetime(self):
		"""Objective: functional test to check that scheduled datetime set/get operations are consistent"""
		repetition = Repetition()
		new_date = datetime.datetime.today() 
		repetition.set_scheduled_datetime(new_date)
		assert repetition.get_scheduled_datetime() == new_date

	def test_repetition_set_get_done_datetime(self):
		"""Objective: functional test to check that done datetime set/get operations are consistent"""
		repetition = Repetition()
		new_date = datetime.datetime.today() 
		repetition.set_done_datetime(new_date)
		assert repetition.get_done_datetime() == new_date

	def test_repetition_set_get_flashcard(self):
		"""Objective: functional test to check that flashcard set/get operations are consistent"""
		repetition = Repetition()
		flashcard = Flashcard()
		repetition.set_flashcard(flashcard)
		assert repetition.get_flashcard() == flashcard


class TestFlashcard(unittest.TestCase):
	
	def test_flashcard_set_get_answer(self):
		"""Objective: functional test to check that answer set/get operations are consistent
		"""
		new_answer = "bass-guitar"
		card = Flashcard()
		card.set_answer(new_answer)
		assert card.get_answer() == new_answer
		
	def test_flashcard_set_get_question(self):
		"""Objective: functional test to check that questions set/get operations are consistent
		"""
		new_question = "Which electrical instrument has four strings?"
		card = Flashcard()
		card.set_question(new_question)
		assert card.get_question() == new_question
		
	def test_flashcard_set_get_question(self):
		"""Objective: functional test to check that questions set/get operations are consistent
		"""
		new_question = "Which electrical instrument has four strings?"
		card = Flashcard()
		card.set_question(new_question)
		assert card.get_question() == new_question
	
	def test_flashcard_set_get_last_repetition(self):
		"""Objective: functional test to check that last repetition set/get operations are consistent"""
		repetition = Repetition()
		flashcard = Flashcard()
		flashcard.set_last_repetition(repetition)
		assert flashcard.get_last_repetition() == repetition

	def test_flashcard_set_get_next_repetition(self):
		"""Objective: functional test to check that last repetition set/get operations are consistent"""
		repetition = Repetition()
		flashcard = Flashcard()
		flashcard.set_next_repetition(repetition)
		assert flashcard.get_next_repetition() == repetition

		
if __name__ == '__main__':
    unittest.main()