#!/usr/bin/python

from memonerd import DatabaseHandler, RepetitionHandler, Repetition, Flashcard
import random
import datetime

class LearningSimulator(object):
	"""The purpose of the simulator is to determine the following:
	- the impact of forgetting rate on the schedules
	- optimal scheduling
	- optimal grading
	- optimal workload
	- repetition priorities
	- database size
	"""
	MIN_REPETITIONS_PER_DAY = 50
	MAX_REPETITIONS_PER_DAY = 120
	NEW_FLASHCARDS_PER_DAY = 10
	MIN_REPETITION_DURATION = 5
	MAX_REPETITION_DURATION = 10
	RETENTION_RATE = 0.8
	_current_date = None
	
	def __init__(self):
		self._current_date = datetime.datetime.now()
		self._dbh = DatabaseHandler()
		self._rbh = RepetitionHandler()
		pass

	def get_random_flashcard(self):
		flashcard = Flashcard()
		token = random.randint(0, 100000)
		answer = "answer-%d" % (token)
		question = "question-%d" % (token)
		flashcard.set_answer(answer)
		flashcard.set_question(question)
		return flashcard

	def create_new_flashcards_for_the_current_date(self):
		for _ in range(0, 10):
			flashcard = self.get_random_flashcard()

	def increment_current_date(self):
		delta = datetime.timedelta(seconds=86400)
		self._current_date += delta
		
	def run(self):
		pass

def main():
	pass

if __name__ == "__main__":
	main()